from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


@app.get("/poll/{environment}")
def poll_env(environment: str):
    print(environment)
    return environment


class Action(BaseModel):
    name: str
    flags: list[str]
    arguments: list[tuple[str, str]]


def poll_queue(environment: str) -> Action | None:
    return Action(name="update", flags=[], arguments=[])
