# deploybot-cli

## Usage

```yaml
script:
  - >- 
    deploybot
    --api-key "${DEPLOYBOT_API_KEY}"
    --server "${DEPLOYBOT_URL}"
    --environment "${CI_ENVIRONMENT_NAME}"
    --project "${CI_PROJECT_NAME}"
    --commit "${CI_COMMIT_SHA}"
```
