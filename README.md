# Deploy Bot
Enable deployments to servers that are behind a firewall

## Overview

- **deploybot-agent:** Runs behind the firewall and polls for actions and executes them
- **deploybot-cli:** Runs on the GitLab runner and pushes new actions
- **deploybot-server:** Handles the communication between the runner and the agents

## Architecture
